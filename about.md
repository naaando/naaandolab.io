---
layout: default
title: Sobre
permalink: /about/
---

# Quem sou
Fernando da Silva Sousa, estudante de eng. de produção(Universidade Iguaçu - Unig).

# O site
Esse é um ~~site~~ teste feito com Jekyll a titulo de esperimentação, será mantido como minha plataforma de blog.

<!--
**Texto original**

This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jekyll" %} /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll) -->
