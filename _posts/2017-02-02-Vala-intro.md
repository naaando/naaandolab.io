---
layout: post
title:  "Introdução ao vala!"
author: Fernando da Silva Sousa
date:   2017-02-02 01:10:00 -0200
categories: programação vala
---
*Vala* é uma linguagem orientada a objetos que apareceu nos meádos de 2006, possui forte influência por suas antecessoras crônológicas *C, C++, Java* e *C#* seu compilador não gera bytecode, invés disso, gera código C usando o sistema GObjects para estender sua funcionalidade. Essa publicação deduz que você já tem um certo grau de conhecimento de programação, se atentando em dar ênfase a Orientação à objetos.

## Olá Mundo!
O famoso hello world, é como um rito de passagem para uma nova linguagem.

{% highlight C# %}

int static main (string[] args) {

	// stdout é o namespace
	stdout.printf("Olá Mundo");
	return 0;

}
{% endhighlight %}

## Funções

Relembrando, as funções do *Vala*, tem um forte apelo visual matemático, fazendo uso de parenteses (), colchetes [] e chaves {}, e não podem ser sobrecarregadas( #TRISTE ).

As funções podem ser:
-   # **Funções sem argumentos**

As funções sem argumentos podem gerar código mais limpo, evitar a duplicação de código e limitar o escopo da funcionalidade tornando mais fácil encontrar possíveis erros em funcionalidades específicas, não hesite em usa-lás.

{% highlight C# %}
void imprimir_na_tela() {
	stdout.printf("Imprimindo na tela");
}
{% endhighlight %}

-   #### **Funções que passam argumentos**

{% highlight C# %}
string bem_vindo() {
	string msg = "Bem vindo ao tutorial de *Vala*";
	return msg;
}
{% endhighlight %}


-   #### **Funções que recebem argumentos**

{% highlight C# %}
void mostrador_velocidade(float velocidade) {
	// As strings em *Vala* podem ser concatenadas
	// Tipos primitivos podem ter uma representação literal acessando o método .to_string();
	stdout.printf("A velocidade é :" + velocidade.to_string());
}
{% endhighlight %}

-   #### **Funções que recebem e passam argumentos**

{% highlight C# %}
int peso(int massa) {
	//Sempre especifique de onde vem os números no seu programa... [Numeros magicos](https://pt.wikipedia.org/wiki/N%C3%BAmero_m%C3%A1gico_(inform%C3%A1tica))
	gravidade = 9.80665;
	int temp = massa*gravidade;
	return temp;
}
{% endhighlight %}


## Tipos de variáveis

Os tipos básicos de *Vala* mais usados são,

-   void
-   int
-   float
-   double
-   char
-   string
-   enum
-   struct
