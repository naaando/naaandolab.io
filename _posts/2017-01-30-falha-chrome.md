---
layout: post
title:  "Falha google chrome expõe dados do usuário!"
author: Fernando da Silva Sousa
date:   2017-01-30 16:21:37 -0200
categories: security
---
Foi publicada no github a demonstração de uma falha de segurança que pode expor informações pessoais como endereço, telefone, cpf, etc.

A falha ocorre no Google Chrome que dispõe de um recurso de preenchimento automático de campo, assim mesmo os formulários ocultos aos usuários acabam por ser preenchidos, podendo enviar dados pessoais sem o consentimento do usuário.

![Comportamento do Google Chrome](https://github.com/anttiviljami/browser-autofill-phishing/raw/master/autofill-demo.gif)

## Outros browsers como firefox e safari não são afetados.

Mais informações e uma demonstração da falha podem ser encontrados no [github](https://github.com/anttiviljami/browser-autofill-phishing)

O problema já existe desde 2012, mas nunca foi considerado uma vunerabilidade.
-   [Why you should not use autocomplete - Yoast](https://yoast.com/autocomplete-security/)

# Como desativar o recurso

*   Vá até configurações ou settings, dependendo do idioma.
*   Desça a pagina, clique em Mostrar configurações avançadas
*   Em senhas e formulários, desmarque Ativar preenchimento automático
