---
layout: home
title: Home
strings: [ "Linux", "Programação", "Início" ]
---
# Ultimos projetos

*Em desenvolvimento*

# Postagens do blog

{% for post in site.posts %}
  <li>
    <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

    <h2>
      <a class="post-link" href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>
    </h2>
  </li>
{% endfor %}
